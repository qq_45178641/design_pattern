# 设计模式学习
## 创建型设计模式
### 简单工厂 [文章简述](https://blog.csdn.net/qq_45178641/article/details/137203465)
### 抽象工厂 [文章简述](https://blog.csdn.net/qq_45178641/article/details/137220713?spm=1001.2014.3001.5501)
### 建造者 [文章简述](https://blog.csdn.net/qq_45178641/article/details/137264653?spm=1001.2014.3001.5501)
### 单例模式 [文章简述](https://blog.csdn.net/qq_45178641/article/details/137327542?spm=1001.2014.3001.5502)
### 原型模式 [文章简述](https://blog.csdn.net/qq_45178641/article/details/137332052?spm=1001.2014.3001.5502)
## 结构型设计模式
### 适配器模式[文章简述](https://blog.csdn.net/qq_45178641/article/details/137368816?spm=1001.2014.3001.5501)
### 桥接模式[文章简述](https://blog.csdn.net/qq_45178641/article/details/137418043?spm=1001.2014.3001.5502)
### 组合模式[文章简述](https://blog.csdn.net/qq_45178641/article/details/137418994?spm=1001.2014.3001.5502)  
### 装饰器模式[文章简述](https://blog.csdn.net/qq_45178641/article/details/137455278?spm=1001.2014.3001.5501)
### 享元模式[文章简述](https://blog.csdn.net/qq_45178641/article/details/137535168?spm=1001.2014.3001.5501)
### 代理模式[文章简述](https://blog.csdn.net/qq_45178641/article/details/137557587?spm=1001.2014.3001.5501)
### 责任链模式[文章简述](https://blog.csdn.net/qq_45178641/article/details/137579978?spm=1001.2014.3001.5502)