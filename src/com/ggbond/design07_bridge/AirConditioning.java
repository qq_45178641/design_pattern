package com.ggbond.design07_bridge;

/**
 * @author ggbond
 * @date 2024年04月05日 16:56
 * 设备实体-空调
 */
public class AirConditioning extends  Machine{
    @Override
    public void start() {
        System.out.println("空调 开机");
    }

    @Override
    public void shutdown() {
        System.out.println("空调 关机");
    }

    @Override
    public void setVolume(int value) {
        System.out.println("空调设置温度大小");
    }
}
