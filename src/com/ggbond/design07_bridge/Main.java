package com.ggbond.design07_bridge;

/**
 * @author ggbond
 * @date 2024年04月05日 17:12
 */
public class Main {
    public static void main(String[] args) {
        Controller c1=new AirController(new AirConditioning());
        Controller c2=new TVController(new TV());
        c1.cStart();//调用 实现层 空调的接口 启动空调
        c2.cStart();//调用 实现层 电视的接口 启动电视
    }
}
