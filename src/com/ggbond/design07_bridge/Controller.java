package com.ggbond.design07_bridge;

/**
 * @author ggbond
 * @date 2024年04月05日 16:43
 */
public abstract class Controller {
    protected  Machine machine;

    public Controller(Machine machine) {
        this.machine = machine;
    }
    //控制器（遥控 开机 ）
    public abstract void cStart();
    //控制器（遥控 机器设备中 一些参数大小 ）
    public  abstract  void  vUp();
    public  abstract  void  vdown();

}
