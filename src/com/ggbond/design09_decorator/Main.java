package com.ggbond.design09_decorator;

/**
 * @author ggbond
 * @date 2024年04月07日 10:15
 */
public class Main {
    public static void main(String[] args) {
         Iheadphone p1=new HeadPhoneSony("xxx型号"); //普通耳机
         Iheadphone p2=new SuperHeadPhone(p1);//不改变p1内部代码的同时,增加降噪功能

         p1.play();
         System.out.println("-----");
         p2.play();

    }
}
