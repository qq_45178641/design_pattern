package com.ggbond.design09_decorator;

/**
 * @author ggbond
 * @date 2024年04月07日 10:14
 * 装饰器  装饰普通耳机 增加降噪功能
 */
public class SuperHeadPhone extends  Iheadphone {
    protected  Iheadphone iheadphone;

    public SuperHeadPhone(Iheadphone iheadphone) {

        this.iheadphone = iheadphone;
    }

    @Override
    public String getName() {
        return iheadphone.getName();
    }

    @Override
    public void play() {
        iheadphone.play();
        System.out.println("开启降噪模式");
    }
}
