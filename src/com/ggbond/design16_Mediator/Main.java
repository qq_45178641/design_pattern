package com.ggbond.design16_Mediator;

/**
 * @author ggbond
 * @date 2024年04月14日 17:09
 */
public class Main {
    public static void main(String[] args) {
        //中介信息
        Mediator mediator=new HouseMediator();

        //组件对象
        Person p1=new Person01("ggbond1",mediator);
        Person p2=new Person01("ggbond2",mediator);

        mediator.add(p1);
        mediator.add(p2);

        p1.send();
        System.out.println("===========");
        p2.receive();

    }
}
