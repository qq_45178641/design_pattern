package com.ggbond.design16_Mediator;

/**
 * @author ggbond
 * @date 2024年04月14日 16:52
 */
public abstract class Mediator {
    //

    abstract void info(Person p1);
    abstract void add(Person p1);
}
