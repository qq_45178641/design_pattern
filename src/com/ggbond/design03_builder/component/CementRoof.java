package com.ggbond.design03_builder.component;

import com.ggbond.design03_builder.Icomponent;

import java.math.BigDecimal;

/**
 * @author ggbond
 * @date 2024年04月02日 09:36
 * 水泥屋顶
 */
public class CementRoof implements Icomponent {
    @Override
    public String position() {
        return "屋顶";
    }

    @Override
    public String type() {
        return "水泥屋顶";
    }

    @Override
    public BigDecimal price() {
        return new BigDecimal(50);
    }
}
