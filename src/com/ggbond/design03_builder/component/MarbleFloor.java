package com.ggbond.design03_builder.component;

import com.ggbond.design03_builder.Icomponent;

import java.math.BigDecimal;

/**
 * @author ggbond
 * @date 2024年04月02日 09:36
 * 大理石地砖
 */
public class MarbleFloor implements Icomponent {
    @Override
    public String position() {
        return "地板";
    }

    @Override
    public String type() {
        return "大理石地砖";
    }

    @Override
    public BigDecimal price() {
        return new BigDecimal(20);
    }
}
