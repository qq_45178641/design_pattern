package com.ggbond.design03_builder.component;

import com.ggbond.design03_builder.Icomponent;

import java.math.BigDecimal;

/**
 * @author ggbond
 * @date 2024年04月02日 09:37
 * 平开窗
 */
public class CasementWindow implements Icomponent {
    @Override
    public String position() {
        return "窗户";
    }

    @Override
    public String type() {
        return "平开窗";
    }

    @Override
    public BigDecimal price() {
        return new BigDecimal(120);
    }

}
