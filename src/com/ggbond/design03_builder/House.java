package com.ggbond.design03_builder;

import jdk.jfr.FlightRecorder;
import jdk.jfr.Name;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * @author ggbond
 * @date 2024年04月02日 09:58
 */
public class House implements  Ihouse{

    private HashMap<String,Icomponent> map;
    private  String name;
    private  BigDecimal area;
    private BigDecimal prize=BigDecimal.ZERO;

    private House(){};
    public House(float area,  String name){
        map=new HashMap<>();
        this.area=new BigDecimal(area);
        this.name=name;
    }
    @Override
    public Ihouse addDoor(Icomponent door) {
        map.put("door",door);
        prize=prize.add(door.price());
        return this;
    }

    @Override
    public Ihouse addFloor(Icomponent floor) {
        map.put("floor", floor);
        prize=prize.add(area.multiply(floor.price()));
        return this;
    }

    @Override
    public Ihouse addWindow(Icomponent window) {
        map.put("window", window);
        prize=prize.add(area.multiply(window.price()));
        return this;
    }

    @Override
    public Ihouse addRoof(Icomponent roof) {
        map.put("roof", roof);
        prize=prize.add(area.multiply(roof.price()));
        return this;
    }
    @Override
    public String getInfo(){
        StringBuilder info=new StringBuilder(
                "房子名称："+name+"\r\n"+
                "装修面积："+area+"\r\n"+
                "装修材料如下："+"\r\n"+
                map.get("door").type()+"\r\n"+
                map.get("window").type()+"\r\n"+
                map.get("roof").type()+"\r\n"+
                map.get("floor").type()+"\r\n"+
                "装修费用共计："+prize+" 元"+"\r\n"+
                "---------------------------"+"\r\n");
        return  info.toString();
    }
}
