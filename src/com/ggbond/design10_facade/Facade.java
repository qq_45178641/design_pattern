package com.ggbond.design10_facade;

/**
 * @author ggbond
 * @date 2024年04月08日 10:43
 */
public class Facade {
    private Delivery delivery;
    private  Lottery lottery;

    public Facade( Lottery lottery,Delivery delivery) {
        this.delivery = delivery;
        this.lottery = lottery;
    }

    public void  toLottery(){
       String id= lottery.getId();//抽奖
        boolean f= lottery.stockControl(id); //库存管理
        if(true==f) {
            delivery.createSaleId(id);//生成订单信息
            delivery.send(id); //发货
        }

    }
}
