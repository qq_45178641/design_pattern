package com.ggbond.design06_adapter;
/**
适配器—— Target 是调用目标， 是其他对象 的"榜样"，
 都要向其看齐
*/
public interface Target {
    void targetUse1();

    void targetUse2();
}
