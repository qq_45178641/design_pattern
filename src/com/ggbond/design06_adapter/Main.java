package com.ggbond.design06_adapter;

/**
 * @author ggbond
 * @date 2024年04月04日 10:43
 * 调用Target接口，使用Adaptee接口
 */
public class Main {
    public static void main(String[] args) {
        Target target01 = new Adapter01();
        target01.targetUse1();
        target01.targetUse2();

        System.out.println("-----------");

        Target target02 = new Adapter02(new Adaptee());
        target02.targetUse1();
        target02.targetUse2();
    }
}
