package com.ggbond.design06_adapter;

/**
 * @author ggbond
 * @date 2024年04月04日 10:29
 * 类适配器 ，采用继承方式
 */
public class Adapter01 extends  Adaptee implements Target {
    @Override
    public void targetUse1() {
        System.out.println("适配 Adaptee 的方法1.");
        method1();
    }

    @Override
    public void targetUse2() {
        System.out.println("适配 Adaptee 的方法2.");
        method2();
    }
}
