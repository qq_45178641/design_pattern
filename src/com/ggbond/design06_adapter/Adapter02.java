package com.ggbond.design06_adapter;

        /**
         * @author ggbond
         * @date 2024年04月04日 10:29
         * 对象适配器，采用继承方式
         */
        public class Adapter02 implements Target  {
            private  Adaptee adaptee;

            public Adapter02(Adaptee adaptee) {
                this.adaptee = adaptee;
            }

            @Override
            public void targetUse1() {
                System.out.println("适配 Adaptee 的方法1.");
                adaptee.method1();
            }

            @Override
            public void targetUse2() {
                System.out.println("适配 Adaptee 的方法2.");
                adaptee.method2();
            }
        }
