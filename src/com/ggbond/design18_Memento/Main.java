package com.ggbond.design18_Memento;

import java.util.Date;

/**
 * @author ggbond
 * @date 2024年04月16日 09:23
 */
public class Main {
    public static void main(String[] args) {
        //记录操作者
        Originator originator=new Originator();
        originator.setLogFile(new LogFile("file_ggbond1",30,new Date(),1));
        //备份中心
        Administrator administrator=new Administrator();
        administrator.add(originator.save());//备份

        originator.setLogFile(new LogFile("file_ggbond2",31,new Date(),2));
        administrator.add(originator.save());//备份

        originator.setLogFile(new LogFile("file_ggbond3",33,new Date(),3));
        administrator.add(originator.save());//备份

        //回滚
        originator.getMemento(administrator.undo());
        System.out.println(originator.getLogFile().toString());

        //再回滚
        originator.getMemento(administrator.undo());
        System.out.println(originator.getLogFile().toString());
        //再回滚
        originator.getMemento(administrator.undo());
        System.out.println(originator.getLogFile().toString());

        //前进版本
        originator.getMemento(administrator.redo());
        System.out.println(originator.getLogFile().toString());

    }
}
