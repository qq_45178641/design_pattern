package com.ggbond.design18_Memento;

/**
 * @author ggbond
 * @date 2024年04月16日 08:42
 *  记录操作者， 能对记录获取与修改的同时，进行记录保存为快照与依据快照进行进行恢复数据
 */
public class Originator {
    private  LogFile logFile;

    public LogFile getLogFile() {
        return logFile;
    }

    public void setLogFile(LogFile logFile) {
        this.logFile = logFile;
    }

    //保存为快照
    public Memento save(){
        return  new Memento(logFile);
    }
    //依据快照恢复现在操作的文件
    public void getMemento(Memento memento) {
        this.logFile=memento.getLogFile();
    }
}
