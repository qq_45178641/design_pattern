package com.ggbond.design18_Memento;

/**
 * @author ggbond
 * @date 2024年04月16日 08:40
 * 备忘录类，在源数据LogFile进行封装扩展，也称快照信息。。
 */
public class Memento {
    private LogFile logFile;

    public Memento(LogFile logFile) {
        this.logFile = logFile;
    }

    public LogFile getLogFile() {
        return logFile;
    }

    public void setLogFile(LogFile logFile) {
        this.logFile = logFile;
    }
}
