package com.ggbond.design01CreationalPattern.impl;

import com.ggbond.design01CreationalPattern.Bed;

/**
 * @author ggbond
 * @date 2024年03月31日 15:44
 */
public class WoodBed implements Bed {
    @Override
    public void getInfo() {
        System.out.println("生产了一个实木床");
    }
}
