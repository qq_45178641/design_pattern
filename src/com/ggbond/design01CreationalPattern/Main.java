package com.ggbond.design01CreationalPattern;

import com.ggbond.design01CreationalPattern.impl.IronBed;

/**
 * @author ggbond
 * @date 2024年03月31日 15:57
 */
public class Main {
    public static void main(String[] args) throws Exception {
        BedFactory factory=new BedFactory();
        Bed ironBed = factory.product("IronBed");
        ironBed.getInfo();
        System.out.println("----------");

        Bed rattanBed = factory.product("RattanBed");
        rattanBed.getInfo();
        System.out.println("----------");

        Bed woodBed = factory.product("WoodBed");
        rattanBed.getInfo();
        System.out.println("----------");


    }
}
