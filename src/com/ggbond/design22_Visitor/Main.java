package com.ggbond.design22_Visitor;

/**
 * @author ggbond
 * @date 2024年04月19日 09:16
 */
public class Main {
    public static void main(String[] args) {
        ObjectStructure obj=new ObjectStructure();
        obj.addElement(new ElementA("ggbond 学Java.doc","write"));
        obj.addElement(new ElementA("ggbond Java目录","read"));

        //不同访问者权限不一样
        obj.visit(new Normal());
        System.out.println("------------");
        obj.visit(new Root());
    }
}
