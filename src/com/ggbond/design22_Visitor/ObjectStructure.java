package com.ggbond.design22_Visitor;

import java.util.LinkedList;
import java.util.List;

/**
 * @author ggbond
 * @date 2024年04月19日 09:13
 * 包含 全部 具体要素
 */
public class ObjectStructure {
    private List<Element> elementList = new LinkedList<>();

    void addElement(Element element) {
        elementList.add(element);
    }
    // 展示该电脑中的文件和文件夹
    void visit(Visitor visitor) {
        for (Element element: elementList) {
            element.accept(visitor);
        }
    }
}
