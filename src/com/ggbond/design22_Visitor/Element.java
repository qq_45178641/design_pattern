package com.ggbond.design22_Visitor;

public interface Element {
    void accept(Visitor v);
}
