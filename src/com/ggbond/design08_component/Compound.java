package com.ggbond.design08_component;

import java.awt.*;

/**
 * @author ggbond
 * @date 2024年04月06日 08:54
 * 部门有：二级部门（下面管三级部门） 三级部门 （无子部门）
 */
public abstract class Compound {
    private String name; // 部门

    public Compound(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public abstract void add(Compound component); // 添加子部门
    public abstract void remove(Compound component); // 删除子部门
    public abstract void select(int depth); // 查看全部子部门
}
