package com.ggbond.design12_Proxy.dynamicProxy;


import net.sf.cglib.proxy.Enhancer;

import java.lang.reflect.Proxy;

/**
 * @author ggbond
 * @date 2024年04月09日 16:00
 */
public class Main {
    public static void main(String[] args) {
        Isend s1= new Send();
        s1.toSend();
        System.out.println("-----------");

        //JDK proxy 基于接口 代理
        Isend JDKproxy=(Isend) Proxy.newProxyInstance(s1.getClass().getClassLoader(),
                                s1.getClass().getInterfaces(),
                                new myIvocationhandler(s1));
        JDKproxy.toSend();
        System.out.println("-----------");

        //cjlib proxy  继承代理
        Send s2 = new Send();
        Enhancer enhancer=new Enhancer();
        enhancer.setCallback(new myInterceptor(s2));
        enhancer.setSuperclass(s2.getClass());
        Send cglibproxy =(Send) enhancer.create();
        cglibproxy.toSend();

    }
}
