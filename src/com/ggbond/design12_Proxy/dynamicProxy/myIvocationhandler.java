package com.ggbond.design12_Proxy.dynamicProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author ggbond
 * @date 2024年04月09日 15:35
 * JDK代理
 */
public class myIvocationhandler  implements InvocationHandler {
    private   Object target;

    public myIvocationhandler(Object target) {
        this.target = target;
    }
    public  void  before(){
        System.out.println("JDK代理人员即将进行配送");
    }

    public  void  after(){
        System.out.println("JDK代理人完成配送");
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object object;
        if ("toSend".equals(method.getName())){ //反射拦截注入
            before();
            object = method.invoke(target, args);
            after();
        }else {
            object = method.invoke(target, args);
        }
        return  object;
    }
}
