package com.ggbond.design12_Proxy.staticProxy;

/**
 * @author ggbond
 * @date 2024年04月09日 15:27
 * 静态代理
 */
public class Main {
    public static void main(String[] args) {
        Isend s1=new Send();
        s1.toSend();
        System.out.println("----------");
        Isend proxy=new staticProxy(s1);
        proxy.toSend();
    }
}
