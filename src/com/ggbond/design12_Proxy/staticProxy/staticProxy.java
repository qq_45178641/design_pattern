package com.ggbond.design12_Proxy.staticProxy;

/**
 * @author ggbond
 * @date 2024年04月09日 15:24
 * 静态代理 类
 */
public class staticProxy implements Isend{
    private Isend send;

    public staticProxy(Isend send) {
        this.send = send;
    }
    //方法增强
    @Override
    public void toSend() {
        before();
        send.toSend();
        after();
    }
    public  void  before(){
        System.out.println("代理人员即将进行配送");
    }

    public  void  after(){
        System.out.println("代理人完成配送");
    }
}
