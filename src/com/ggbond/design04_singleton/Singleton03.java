package com.ggbond.design04_singleton;

/**
 * @author ggbond
 * @date 2024年04月03日 07:53
 * 类的内部类  线程安全  JVM保证访问安全，一个类的构造方法在多线程环境下能被正确地加载
 */
public class Singleton03 {

    private  static class Singleton {
        private static Singleton03 instance=new Singleton03();
    }
    private Singleton03(){};
    public  static  Singleton03 getInstance(){
        return  Singleton.instance;
    }
}
