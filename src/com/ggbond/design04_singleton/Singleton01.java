package com.ggbond.design04_singleton;

/**
 * @author ggbond
 * @date 2024年04月03日 07:47
 * 懒汉式 线程安全
 */
public class Singleton01 {
    //类加载时初始化
    private final static Singleton01 instance=new Singleton01();

    private Singleton01(){};

    public  static Singleton01 getInstance(){
        return  instance;
    }
}
