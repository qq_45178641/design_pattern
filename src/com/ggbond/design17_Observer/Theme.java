package com.ggbond.design17_Observer;

import java.util.Observer;

/**
 * @author ggbond
 * @date 2024年04月15日 10:04
 */
public  interface Theme {
    // 添加观察者（订阅者）
    public void attach(Subscribe s);
    // 删除观察者（订阅者）
    public void detach(Subscribe s);
    // 通知所有观察者（订阅者）
    public void notifySubscriber();
}
