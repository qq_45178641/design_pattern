package com.ggbond.design17_Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

/**
 * @author ggbond
 * @date 2024年04月15日 10:06
 */
public class Publisher implements Theme{
    private List<Subscribe> list=new ArrayList<>();
    @Override
    public void attach(Subscribe s) {
        list.add(s);

    }

    @Override
    public void detach(Subscribe s) {
        list.remove(s);
    }

    //通知所有订阅者
    @Override
    public void notifySubscriber() {
        System.out.println("@全体人员，今晚有个party，地点在望子成龙小学");
        for (Subscribe o : list) {
            o.getinfo();
        }
    }
}
