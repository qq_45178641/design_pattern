package com.ggbond.design13_ChainofRes;

import java.util.ArrayList;

/**
 * @author ggbond
 * @date 2024年04月10日 08:13
 */
public class Main {
    public static void main(String[] args) {
        Question q1=new Question(1,"1+1=");
        Question q2=new Question(2,"数据结构与算法");
        Question q3=new Question(3,"高等算法");
        Question q4=new Question(4,"LLM");
        ArrayList<Question> list=new ArrayList<>();
        list.add(q1);list.add(q2);list.add(q3); list.add(q4);

        Handler h1= new Child("儿童");
        Handler h2= new Undergraduate("大学生");
        Handler h3= new Graduates("研究生");

        h1.setNextHandler(h2);
        h2.setNextHandler(h3);

        for ( Question item: list) {
             h1.handle(item);
        }
    }
}
