package com.ggbond.design13_ChainofRes;

/**
 * @author ggbond
 * @date 2024年04月10日 07:48
 */
public class Question {
    private  int level;

    private  String detail;

    public Question(int level, String detail) {
        this.level = level;
        this.detail = "这是难度系数为"+String.valueOf(level)+":"+detail;
    }

    public int getLevel() {
        return level;
    }

    public String getDetail() {
        return detail;
    }
}
