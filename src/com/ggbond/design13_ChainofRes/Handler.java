package com.ggbond.design13_ChainofRes;

/**
 * @author ggbond
 * @date 2024年04月10日 07:53
 */
public abstract class Handler {
    final static int LEVEL1 = 1; // 题目难度为1
    final static int LEVEL2 = 2; // 题目难度为2
    final static int LEVEL3 = 3; // 题目难度为3
    // 能处理的级别
    private int level = 0;
    // 责任传递，下一个责任人是谁
    private Handler nextHandler;
    protected   String name;

    public Handler(int level) {
        this.level = level;
    }

    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    protected abstract void response(Question question);

    final void handle(Question question) {
        // 难度小递增处理
        if (question.getLevel() <= this.level) {
            this.response(question);
        } else {
            if (this.nextHandler != null) { // 有后续环节，请求传递
                this.nextHandler.handle(question); // 责任传递
            } else { // 无后续环节了，无法处理
                System.out.print("题目难度为" + question.getLevel() +" ");
                System.out.println("，这题目太难，没有人能可以处理。");
            }
        }
    }
}
