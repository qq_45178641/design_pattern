package com.ggbond.design05_prototype.deepClone;


import java.util.Date;

/**
 * @author ggbond
 * @date 2024年04月03日 08:42
 * 原型模式，本质是克隆复制对象。
 */
public class test02 {
    public static void main(String[] args) throws Exception {
        Date birth=new Date(2022,4,3);
        int age=25;
        Mankind02 m1=new Mankind02(age,birth);
        Mankind02 m2=m1.clone();
        System.out.println("m1.age:"+m1.getAge()+"  "+"m2.age:"+m2.getAge());
        System.out.println("m1.birth:"+m1.getBirth()+"  "+"m2.birth:"+m2.getBirth());
        System.out.println("-----------");

        age=21; birth.setTime(123232321L);

        System.out.println(m1.getBirth() == m2.getBirth()); // true
        System.out.println("m1.age:"+m1.getAge()+"  "+"m2.age:"+m2.getAge());
        System.out.println("m1.birth:"+m1.getBirth()+"  "+"m2.birth:"+m2.getBirth());

    }
}
