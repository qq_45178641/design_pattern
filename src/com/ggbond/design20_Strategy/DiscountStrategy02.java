package com.ggbond.design20_Strategy;

/**
 * @author ggbond
 * @date 2024年04月18日 08:08
 */
public class DiscountStrategy02 implements Istrategy {
    @Override
    public void discount() {
        System.out.println("全场西瓜打9折");
    }
}
