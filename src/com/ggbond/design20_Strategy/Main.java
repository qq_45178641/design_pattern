package com.ggbond.design20_Strategy;

/**
 * @author ggbond
 * @date 2024年04月18日 08:13
 */
public class Main {
    public static void main(String[] args) {
        Context context = new Context();
        Istrategy s1=new DiscountStrategy01();
        Istrategy s2=new DiscountStrategy02();
        Istrategy s3=new DiscountStrategy03();
        //设置策略
        context.setStrategy(s1);
        context.discount();
        //更换策略
        context.setStrategy(s2);
        context.discount();

        context.setStrategy(s3);
        context.discount();
    }
}
