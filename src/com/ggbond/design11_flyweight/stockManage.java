package com.ggbond.design11_flyweight;

/**
 * @author ggbond
 * @date 2024年04月09日 08:19
 */
public class stockManage {
    private int saleNum; //共计数量

    private int  soldNum; //已卖数量

    public stockManage(int saleNum, int soldNum) {
        this.saleNum = saleNum;
        this.soldNum = soldNum;
    }

    public int getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(int saleNum) {
        this.saleNum = saleNum;
    }

    public int getSoldNum() {
        return soldNum;
    }

    public void setSoldNum(int soldNum) {
        this.soldNum = soldNum;
    }

    @Override
    public String toString() {
        return "stockManage{" +
                "saleNum=" + saleNum +
                ", soldNum=" + soldNum +
                '}';
    }
}
