package com.ggbond.design11_flyweight;

/**
 * @author ggbond
 * @date 2024年04月09日 08:22
 */
public class Main {
    public static void main(String[] args) {
        //数据库获取商品静态信息
        Commodity commodity = CommodityFactory.getCommodity(1l);
        //redis等获取实时 商品库存信息
        stockManage stockManage=new stockManage(100,50);

        //商品信息组合
        commodity.setStockManage(stockManage);

        //返回给客户端
        System.out.println(commodity.toString());

    }
}
