package com.ggbond.design11_flyweight;

import com.ggbond.design08_component.Composite;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ggbond
 * @date 2024年04月09日 08:23
 */
public class CommodityFactory {
    private static Map<Long,Commodity> map=new HashMap<>();
    public static Commodity getCommodity(Long cid){
        if (map.containsKey(cid)) {
            return map.get(cid);
        }else {
            //查询数据库返回
            Commodity commodity=new Commodity(2l,"耳机","电子产品","001","这是耳机");
            return  commodity;
        }
    }
}
