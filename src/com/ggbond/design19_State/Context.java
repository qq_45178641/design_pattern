package com.ggbond.design19_State;

/**
 * @author ggbond
 * @date 2024年04月17日 08:29
 */
public class Context {
    private  State state;
    public void play(){
        this.state.play();
    }
    public void  study(){
        this.state.study();
    }
    public void  battle(){
        this.state.battle();
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
