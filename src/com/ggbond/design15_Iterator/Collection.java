package com.ggbond.design15_Iterator;
/**
 * 定义集合
 * */
public interface Collection<A> {
    boolean add(A a);
    boolean remove(A a);
    Iterator<A> createIterator();

}
