package com.ggbond.design15_Iterator;

/**
 * @author ggbond
 * @date 2024年04月13日 09:30
 */
public class Main {
    public static void main(String[] args) {

        Person p1= new Person("ggbond1","001");
        Person p2= new Person("ggbond2","002");
        Person p3= new Person("ggbond3","003");
        Person p4= new Person("ggbond4","004");
        Collection<Person> collection=new MyCollection<>();
        collection.add(p1);
        collection.add(p2);
        collection.add(p3);
        collection.add(p4);
        Iterator<Person> iterator = collection.createIterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
