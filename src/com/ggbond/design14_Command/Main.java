package com.ggbond.design14_Command;

/**
 * @author ggbond
 * @date 2024年04月12日 15:38
 */
public class Main {
    public static void main(String[] args) {
         Stationery s1=new Pencil(new Deil());
         Stationery s2=new WatercolorPen(new ChenGuang());

         //小贩去进货
           Hawker hawker=new Hawker();
           hawker.stock(s1);
           hawker.stock(s2);
           //查看商品
            hawker.all();
    }
}
