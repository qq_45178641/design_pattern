package com.ggbond.design02_abstractFactory.impl;

import com.ggbond.design02_abstractFactory.BedsideTable;

/**
 * @author ggbond
 * @date 2024年04月01日 08:39
 */
public class PaintedBoardBedsideTable implements BedsideTable {
    @Override
    public void getInfo_b() {
        System.out.println("生产了一个 涂漆床头柜");
    }
}
