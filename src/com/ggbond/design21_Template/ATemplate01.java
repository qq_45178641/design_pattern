package com.ggbond.design21_Template;

/**
 * @author ggbond
 * @date 2024年04月18日 17:42
 */
public class ATemplate01  extends ActivityTemplate{
    @Override
    protected void chooseType(String info) {
        System.out.println(info+"已在现场，下午3点直接拿奖品");
    }
}
