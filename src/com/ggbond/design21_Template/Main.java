package com.ggbond.design21_Template;

/**
 * @author ggbond
 * @date 2024年04月18日 17:56
 */
public class Main {
    public static void main(String[] args) {
        //模版定好骨架，流程等
        ActivityTemplate a1=new ATemplate01();
        a1.reward("ggbond1");
        System.out.println("----------------");
        ActivityTemplate a2=new ATemplate02();
        a2.reward("ggbond2");
    }
}
